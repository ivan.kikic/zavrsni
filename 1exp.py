import time
import numpy as np 
import pandas as pd
import statistics
import xlsxwriter
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import seaborn as sns
from sklearn import preprocessing
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report,confusion_matrix
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import make_pipeline
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from scipy.stats import skew
import os
import csv
from csv import reader
import xlrd
print(os.getcwd())


from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier

df=pd.read_csv('G1.csv')


### Deleting all columns where is more than 49% of nan values ###
perc = 49.0
min_count =  int(((100-perc)/100)*df.shape[0] + 1)
df = df.dropna( axis=1, thresh=min_count)

X = df.drop(['label', 'animal_ID'], axis=1)
y = df.iloc[:, 0]

df2 = pd.read_csv('G2.csv')
min_count =  int(((100-perc)/100)*df2.shape[0] + 1)
df2 = df2.dropna( axis=1, thresh=min_count)

df3 = pd.read_csv('S1.csv')
min_count =  int(((100-perc)/100)*df3.shape[0] + 1)
df3 = df3.dropna( axis=1, thresh=min_count)

#X_G2 = pd.read_csv('G2_edited.csv')
#X_G2 = X_G2.drop(['Unnamed: 0','label', 'animal_ID'], axis=1)

#X_S1 = pd.read_csv('S1_edited.csv')
#X_S1 = X_S1.drop(['Unnamed: 0','label', 'animal_ID'], axis=1)

#perc = 45.0
#min_count =  int(((100-perc)/100)*X.shape[1] + 1)
#X = X.dropna( axis=0, 
#                thresh=min_count)


#print("Srednja vrijednost je: ")
#print(df.mean(axis = 0))
#print("Medijan je: ")
#print(df.median())
#print("Min je: ")
#print(df.min())
#print("Max je: ")
#print(df.max())
#print("Standardna devijacija je: ")
#print(df.std())
#print("DONE!")

#sns.boxplot(df['ay'])

#Normalization
#scaler = MinMaxScaler()
#print(scaler.fit_transform(X))

#Standardization
#scaler = StandardScaler()
#print(scaler.fit_transform(X))

#plt.hist(Y, bins = 9)
#plt.show()


book1 = xlsxwriter.Workbook('Rezultati1.xlsx')
sheet1 = book1.add_worksheet()
sheet1.write(0,1, 'LR')
sheet1.write(0,2, '1NN')
sheet1.write(0,3, '5NN')
sheet1.write(0,4, 'RF')
sheet1.write(0,5, 'DT')
sheet1.write(32,0, 'SV')
sheet1.write(33,0, 'MED')
sheet1.write(34,0, 'MIN')
sheet1.write(35,0, 'MAX')
sheet1.write(36,0, 'ST DEV')


book2 = xlsxwriter.Workbook('Rezultati2.xlsx')
sheet2 = book2.add_worksheet()
sheet2.write(0,1, 'LR')
sheet2.write(0,2, '1NN')
sheet2.write(0,3, '5NN')
sheet2.write(0,4, 'RF')
sheet2.write(0,5, 'DT')
sheet2.write(32,0, 'SV')
sheet2.write(33,0, 'MED')
sheet2.write(34,0, 'MIN')
sheet2.write(35,0, 'MAX')
sheet2.write(36,0, 'ST DEV')


book3 = xlsxwriter.Workbook('Rezultati3.xlsx')
sheet3 = book3.add_worksheet()
sheet3.write(0,1, 'LR')
sheet3.write(0,2, '1NN')
sheet3.write(0,3, '5NN')
sheet3.write(0,4, 'RF')
sheet3.write(0,5, 'DT')
sheet3.write(32,0, 'SV')
sheet3.write(33,0, 'MED')
sheet3.write(34,0, 'MIN')
sheet3.write(35,0, 'MAX')
sheet3.write(36,0, 'ST DEV')


X_G2 = df2.sample(n = 529236, random_state=1)
y_test_G2 = X_G2.iloc[:, 0]
X_G2 = X_G2.drop(['label', 'animal_ID'], axis=1)

X_S1 = df3.sample(n = 529236, random_state=1)
y_test_S1 = X_S1.iloc[:, 0]
X_S1 = X_S1.drop(['label', 'animal_ID'], axis=1)

f1_score1 = []
f1_score2 = []
f1_score3 = []
recall_score1 = []
recall_score2 = []
recall_score3 = []
precision_score1 = []
precision_score2 = []
precision_score3 = []

for i in range(30):
    X_train, X_test, y_train, y_test = train_test_split(X, y.values.ravel(), random_state=13+i, stratify=y)

    model = LogisticRegression(solver='lbfgs', max_iter=1000)
    model.fit(X_train, y_train)
    
    y_pred = model.predict(X_test)
    y_pred1 = model.predict(X_G2)
    y_pred2 = model.predict(X_S1)
    
    result0 = model.score(X_test, y_test)
    result1 = f1_score(y_test, y_pred, average='macro', labels=np.unique(y_pred))
    result2 = recall_score(y_test, y_pred, average='macro', labels=np.unique(y_pred))
    result3 = precision_score(y_test, y_pred, average='macro', labels=np.unique(y_pred))
    print("LogReg - Accuracy of iteration no.", i+1 ,": %.2f%%" % (result0*100.0))
    print("LogReg - F1 Score of iteration no.", i+1 ,": %.2f%%" % (result1*100.0))
    print("LogReg - Recall of iteration no.", i+1 ,": %.2f%%" % (result2*100.0))
    print("LogReg - Precision of iteration no.", i+1 ,": %.2f%%" % (result3*100.0))
    print("##############################################################")
    result01 = model.score(X_G2, y_test_G2)
    result11 = f1_score(y_test_G2, y_pred1, average='macro', labels=np.unique(y_pred1))
    result21 = recall_score(y_test_G2, y_pred1, average='macro', labels=np.unique(y_pred1))
    result31 = precision_score(y_test_G2, y_pred1, average='macro', labels=np.unique(y_pred1))
    print("LogReg - Accuracy of iteration no.", i+1 ,": %.2f%%" % (result01*100.0))
    print("LogReg - F1 Score of iteration no.", i+1 ,": %.2f%%" % (result11*100.0))
    print("LogReg - Recall of iteration no.", i+1 ,": %.2f%%" % (result21*100.0))
    print("LogReg - Precision of iteration no.", i+1 ,": %.2f%%" % (result31*100.0))
    print("##############################################################")
    result02 = model.score(X_S1, y_test_S1)
    result12 = f1_score(y_test_S1, y_pred2, average='macro', labels=np.unique(y_pred2))
    result22 = recall_score(y_test_S1, y_pred2, average='macro', labels=np.unique(y_pred2))
    result32 = precision_score(y_test_S1, y_pred2, average='macro', labels=np.unique(y_pred2))
    print("LogReg - Accuracy of iteration no.", i+1 ,": %.2f%%" % (result02*100.0))
    print("LogReg - F1 Score of iteration no.", i+1 ,": %.2f%%" % (result12*100.0))
    print("LogReg - Recall of iteration no.", i+1 ,": %.2f%%" % (result22*100.0))
    print("LogReg - Precision of iteration no.", i+1 ,": %.2f%%" % (result32*100.0))
    print("##############################################################")
    f1_score1.append(result1)
    recall_score1.append(result2)
    precision_score1.append(result3)
    
    f1_score2.append(result11)
    recall_score2.append(result21)
    precision_score2.append(result31)

    f1_score3.append(result12)
    recall_score3.append(result22)
    precision_score3.append(result32)

print("G1 F1 score - srednja vrijednost je: ", statistics.mean(f1_score1))
print("G1 F1 score - medijan je: ",statistics.median(f1_score1))
print("G1 F1 score - min je: ", min(f1_score1))
print("G1 F1 score - max je: ", max(f1_score1))
print("G1 F1 score - standardna devijacija je: ", statistics.stdev(f1_score1))
print(" ")
print("G1 Recall score - srednja vrijednost je: ", statistics.mean(recall_score1))
print("G1 Recall score - medijan je: ", statistics.median(recall_score1))
print("G1 Recall score - min je: ", min(recall_score1))
print("G1 Recall score - max je: ", max(recall_score1))
print("G1 Recall score - standardna devijacija je: ", statistics.stdev(f1_score1))
print(" ")
print("G1 Precision score - srednja vrijednost je: ", statistics.mean(precision_score1))
print("G1 Precision score - medijan je: ", statistics.median(precision_score1))
print("G1 Precision score - min je: ", min(precision_score1))
print("G1 Precision score - max je: ", max(precision_score1))
print("G1 Precision score - standardna devijacija je: ", statistics.stdev(precision_score1))
print("#################################################################")

print("G2 F1 score - srednja vrijednost je: ", statistics.mean(f1_score2))
print("G2 F1 score - medijan je: ",statistics.median(f1_score2))
print("G2 F1 score - min je: ", min(f1_score2))
print("G2 F1 score - max je: ", max(f1_score2))
print("G2 F1 score - standardna devijacija je: ", statistics.stdev(f1_score2))
print(" ")
print("G2 Recall score - srednja vrijednost je: ", statistics.mean(recall_score2))
print("G2 Recall score - medijan je: ", statistics.median(recall_score2))
print("G2 Recall score - min je: ", min(recall_score2))
print("G2 Recall score - max je: ", max(recall_score2))
print("G2 Recall score - standardna devijacija je: ", statistics.stdev(f1_score2))
print(" ")
print("G2 Precision score - srednja vrijednost je: ", statistics.mean(precision_score2))
print("G2 Precision score - medijan je: ", statistics.median(precision_score2))
print("G2 Precision score - min je: ", min(precision_score2))
print("G2 Precision score - max je: ", max(precision_score2))
print("G2 Precision score - standardna devijacija je: ", statistics.stdev(precision_score2))
print("#################################################################")

print("S1 F1 score - srednja vrijednost je: ", statistics.mean(f1_score3))
print("S1 F1 score - medijan je: ",statistics.median(f1_score3))
print("S1 F1 score - min je: ", min(f1_score3))
print("S1 F1 score - max je: ", max(f1_score3))
print("S1 F1 score - standardna devijacija je: ", statistics.stdev(f1_score3))
print(" ")
print("S1 Recall score - srednja vrijednost je: ", statistics.mean(recall_score3))
print("S1 Recall score - medijan je: ", statistics.median(recall_score3))
print("S1 Recall score - min je: ", min(recall_score3))
print("S1 Recall score - max je: ", max(recall_score3))
print("S1 Recall score - standardna devijacija je: ", statistics.stdev(f1_score3))
print(" ")
print("S1 Precision score - srednja vrijednost je: ", statistics.mean(precision_score3))
print("S1 Precision score - medijan je: ", statistics.median(precision_score3))
print("S1 Precision score - min je: ", min(precision_score3))
print("S1 Precision score - max je: ", max(precision_score3))
print("S1 Precision score - standardna devijacija je: ", statistics.stdev(precision_score3))
print("#################################################################")



row1 = 1
column1 = 1
row2 = 1
column2 = 1
row3 = 1
column3 = 1


for i in f1_score1:
    sheet1.write(row1,column1, i)
    row1 += 1

row1 += 1
sheet1.write(row1,column1, statistics.mean(f1_score1))
row1 += 1
sheet1.write(row1,column1, statistics.median(f1_score1))
row1 += 1
sheet1.write(row1,column1, min(f1_score1))
row1 += 1
sheet1.write(row1,column1, max(f1_score1))
row1 += 1
sheet1.write(row1,column1, statistics.stdev(f1_score1))
column1 += 1

for i in f1_score2:
    sheet2.write(row2,column2, i)
    row2 += 1

row2 += 1
sheet2.write(row2,column2, statistics.mean(f1_score2))
row2 += 1
sheet2.write(row2,column2, statistics.median(f1_score2))
row2 += 1
sheet2.write(row2,column2, min(f1_score2))
row2 += 1
sheet2.write(row2,column2, max(f1_score2))
row2 += 1
sheet2.write(row2,column2, statistics.stdev(f1_score2))
column2 += 1

for i in f1_score3:
    sheet3.write(row3,column3, i)
    row3 += 1

row3 += 1
sheet3.write(row3,column3, statistics.mean(f1_score3))
row3 += 1
sheet3.write(row3,column3, statistics.median(f1_score3))
row3 += 1
sheet3.write(row3,column3, min(f1_score3))
row3 += 1
sheet3.write(row3,column3, max(f1_score3))
row3 += 1
sheet3.write(row3,column3, statistics.stdev(f1_score3))
column3 += 1




print("DONE - Logistic Regression!")


###########################################################################################################################################



f1_score1 = []
f1_score2 = []
f1_score3 = []
recall_score1 = []
recall_score2 = []
recall_score3 = []
precision_score1 = []
precision_score2 = []
precision_score3 = []

for i in range(30):
    X_train, X_test, y_train, y_test = train_test_split(X, y.values.ravel(), random_state=13+i, stratify=y)

    model = KNeighborsClassifier(n_neighbors = 1)
    model.fit(X_train, y_train)
    
    y_pred = model.predict(X_test)
    y_pred1 = model.predict(X_G2)
    y_pred2 = model.predict(X_S1)
    
    result0 = model.score(X_test, y_test)
    result1 = f1_score(y_test, y_pred, average='macro', labels=np.unique(y_pred))
    result2 = recall_score(y_test, y_pred, average='macro', labels=np.unique(y_pred))
    result3 = precision_score(y_test, y_pred, average='macro', labels=np.unique(y_pred))
    print("KNN N=1 - Accuracy of iteration no.", i+1 ,": %.2f%%" % (result0*100.0))
    print("KNN N=1 - F1 Score of iteration no.", i+1 ,": %.2f%%" % (result1*100.0))
    print("KNN N=1 - Recall of iteration no.", i+1 ,": %.2f%%" % (result2*100.0))
    print("KNN N=1 - Precision of iteration no.", i+1 ,": %.2f%%" % (result3*100.0))
    print("##############################################################")
    result01 = model.score(X_G2, y_test_G2)
    result11 = f1_score(y_test_G2, y_pred1, average='macro', labels=np.unique(y_pred1))
    result21 = recall_score(y_test_G2, y_pred1, average='macro', labels=np.unique(y_pred1))
    result31 = precision_score(y_test_G2, y_pred1, average='macro', labels=np.unique(y_pred1))
    print("KNN N=1 - Accuracy of iteration no.", i+1 ,": %.2f%%" % (result01*100.0))
    print("KNN N=1 - F1 Score of iteration no.", i+1 ,": %.2f%%" % (result11*100.0))
    print("KNN N=1 - Recall of iteration no.", i+1 ,": %.2f%%" % (result21*100.0))
    print("KNN N=1 - Precision of iteration no.", i+1 ,": %.2f%%" % (result31*100.0))
    print("##############################################################")
    result02 = model.score(X_S1, y_test_S1)
    result12 = f1_score(y_test_S1, y_pred2, average='macro', labels=np.unique(y_pred2))
    result22 = recall_score(y_test_S1, y_pred2, average='macro', labels=np.unique(y_pred2))
    result32 = precision_score(y_test_S1, y_pred2, average='macro', labels=np.unique(y_pred2))
    print("KNN N=1 - Accuracy of iteration no.", i+1 ,": %.2f%%" % (result02*100.0))
    print("KNN N=1 - F1 Score of iteration no.", i+1 ,": %.2f%%" % (result12*100.0))
    print("KNN N=1 - Recall of iteration no.", i+1 ,": %.2f%%" % (result22*100.0))
    print("KNN N=1 - Precision of iteration no.", i+1 ,": %.2f%%" % (result32*100.0))
    print("##############################################################")
    f1_score1.append(result1)
    recall_score1.append(result2)
    precision_score1.append(result3)
    
    f1_score2.append(result11)
    recall_score2.append(result21)
    precision_score2.append(result31)

    f1_score3.append(result12)
    recall_score3.append(result22)
    precision_score3.append(result32)

print("G1 F1 score - srednja vrijednost je: ", statistics.mean(f1_score1))
print("G1 F1 score - medijan je: ",statistics.median(f1_score1))
print("G1 F1 score - min je: ", min(f1_score1))
print("G1 F1 score - max je: ", max(f1_score1))
print("G1 F1 score - standardna devijacija je: ", statistics.stdev(f1_score1))
print(" ")
print("G1 Recall score - srednja vrijednost je: ", statistics.mean(recall_score1))
print("G1 Recall score - medijan je: ", statistics.median(recall_score1))
print("G1 Recall score - min je: ", min(recall_score1))
print("G1 Recall score - max je: ", max(recall_score1))
print("G1 Recall score - standardna devijacija je: ", statistics.stdev(f1_score1))
print(" ")
print("G1 Precision score - srednja vrijednost je: ", statistics.mean(precision_score1))
print("G1 Precision score - medijan je: ", statistics.median(precision_score1))
print("G1 Precision score - min je: ", min(precision_score1))
print("G1 Precision score - max je: ", max(precision_score1))
print("G1 Precision score - standardna devijacija je: ", statistics.stdev(precision_score1))
print("#################################################################")

print("G2 F1 score - srednja vrijednost je: ", statistics.mean(f1_score2))
print("G2 F1 score - medijan je: ",statistics.median(f1_score2))
print("G2 F1 score - min je: ", min(f1_score2))
print("G2 F1 score - max je: ", max(f1_score2))
print("G2 F1 score - standardna devijacija je: ", statistics.stdev(f1_score2))
print(" ")
print("G2 Recall score - srednja vrijednost je: ", statistics.mean(recall_score2))
print("G2 Recall score - medijan je: ", statistics.median(recall_score2))
print("G2 Recall score - min je: ", min(recall_score2))
print("G2 Recall score - max je: ", max(recall_score2))
print("G2 Recall score - standardna devijacija je: ", statistics.stdev(f1_score2))
print(" ")
print("G2 Precision score - srednja vrijednost je: ", statistics.mean(precision_score2))
print("G2 Precision score - medijan je: ", statistics.median(precision_score2))
print("G2 Precision score - min je: ", min(precision_score2))
print("G2 Precision score - max je: ", max(precision_score2))
print("G2 Precision score - standardna devijacija je: ", statistics.stdev(precision_score2))
print("#################################################################")

print("S1 F1 score - srednja vrijednost je: ", statistics.mean(f1_score3))
print("S1 F1 score - medijan je: ",statistics.median(f1_score3))
print("S1 F1 score - min je: ", min(f1_score3))
print("S1 F1 score - max je: ", max(f1_score3))
print("S1 F1 score - standardna devijacija je: ", statistics.stdev(f1_score3))
print(" ")
print("S1 Recall score - srednja vrijednost je: ", statistics.mean(recall_score3))
print("S1 Recall score - medijan je: ", statistics.median(recall_score3))
print("S1 Recall score - min je: ", min(recall_score3))
print("S1 Recall score - max je: ", max(recall_score3))
print("S1 Recall score - standardna devijacija je: ", statistics.stdev(f1_score3))
print(" ")
print("S1 Precision score - srednja vrijednost je: ", statistics.mean(precision_score3))
print("S1 Precision score - medijan je: ", statistics.median(precision_score3))
print("S1 Precision score - min je: ", min(precision_score3))
print("S1 Precision score - max je: ", max(precision_score3))
print("S1 Precision score - standardna devijacija je: ", statistics.stdev(precision_score3))
print("#################################################################")

row1 = 1
column1 = 2
row2 = 1
column2 = 2
row3 = 1
column3 = 2


for i in f1_score1:
    sheet1.write(row1,column1, i)
    row1 += 1

row1 += 1
sheet1.write(row1,column1, statistics.mean(f1_score1))
row1 += 1
sheet1.write(row1,column1, statistics.median(f1_score1))
row1 += 1
sheet1.write(row1,column1, min(f1_score1))
row1 += 1
sheet1.write(row1,column1, max(f1_score1))
row1 += 1
sheet1.write(row1,column1, statistics.stdev(f1_score1))
column1 += 1

for i in f1_score2:
    sheet2.write(row2,column2, i)
    row2 += 1

row2 += 1
sheet2.write(row2,column2, statistics.mean(f1_score2))
row2 += 1
sheet2.write(row2,column2, statistics.median(f1_score2))
row2 += 1
sheet2.write(row2,column2, min(f1_score2))
row2 += 1
sheet2.write(row2,column2, max(f1_score2))
row2 += 1
sheet2.write(row2,column2, statistics.stdev(f1_score2))
column2 += 1

for i in f1_score3:
    sheet3.write(row3,column3, i)
    row3 += 1

row3 += 1
sheet3.write(row3,column3, statistics.mean(f1_score3))
row3 += 1
sheet3.write(row3,column3, statistics.median(f1_score3))
row3 += 1
sheet3.write(row3,column3, min(f1_score3))
row3 += 1
sheet3.write(row3,column3, max(f1_score3))
row3 += 1
sheet3.write(row3,column3, statistics.stdev(f1_score3))
column3 += 1
print("DONE - KNN N=1!")


###########################################################################################################################################



f1_score1 = []
f1_score2 = []
f1_score3 = []
recall_score1 = []
recall_score2 = []
recall_score3 = []
precision_score1 = []
precision_score2 = []
precision_score3 = []

for i in range(30):
    X_train, X_test, y_train, y_test = train_test_split(X, y.values.ravel(), random_state=13+i, stratify=y)

    model = KNeighborsClassifier()
    model.fit(X_train, y_train)
    
    y_pred = model.predict(X_test)
    y_pred1 = model.predict(X_G2)
    y_pred2 = model.predict(X_S1)
    
    result0 = model.score(X_test, y_test)
    result1 = f1_score(y_test, y_pred, average='macro', labels=np.unique(y_pred))
    result2 = recall_score(y_test, y_pred, average='macro', labels=np.unique(y_pred))
    result3 = precision_score(y_test, y_pred, average='macro', labels=np.unique(y_pred))
    print("KNN N=5 - Accuracy of iteration no.", i+1 ,": %.2f%%" % (result0*100.0))
    print("KNN N=5 - F1 Score of iteration no.", i+1 ,": %.2f%%" % (result1*100.0))
    print("KNN N=5 - Recall of iteration no.", i+1 ,": %.2f%%" % (result2*100.0))
    print("KNN N=5 - Precision of iteration no.", i+1 ,": %.2f%%" % (result3*100.0))
    print("##############################################################")
    result01 = model.score(X_G2, y_test_G2)
    result11 = f1_score(y_test_G2, y_pred1, average='macro', labels=np.unique(y_pred1))
    result21 = recall_score(y_test_G2, y_pred1, average='macro', labels=np.unique(y_pred1))
    result31 = precision_score(y_test_G2, y_pred1, average='macro', labels=np.unique(y_pred1))
    print("KNN N=5 - Accuracy of iteration no.", i+1 ,": %.2f%%" % (result01*100.0))
    print("KNN N=5 - F1 Score of iteration no.", i+1 ,": %.2f%%" % (result11*100.0))
    print("KNN N=5 - Recall of iteration no.", i+1 ,": %.2f%%" % (result21*100.0))
    print("KNN N=5 - Precision of iteration no.", i+1 ,": %.2f%%" % (result31*100.0))
    print("##############################################################")
    result02 = model.score(X_S1, y_test_S1)
    result12 = f1_score(y_test_S1, y_pred2, average='macro', labels=np.unique(y_pred2))
    result22 = recall_score(y_test_S1, y_pred2, average='macro', labels=np.unique(y_pred2))
    result32 = precision_score(y_test_S1, y_pred2, average='macro', labels=np.unique(y_pred2))
    print("KNN N=5 - Accuracy of iteration no.", i+1 ,": %.2f%%" % (result02*100.0))
    print("KNN N=5 - F1 Score of iteration no.", i+1 ,": %.2f%%" % (result12*100.0))
    print("KNN N=5 - Recall of iteration no.", i+1 ,": %.2f%%" % (result22*100.0))
    print("KNN N=5 - Precision of iteration no.", i+1 ,": %.2f%%" % (result32*100.0))
    print("##############################################################")
    f1_score1.append(result1)
    recall_score1.append(result2)
    precision_score1.append(result3)
    
    f1_score2.append(result11)
    recall_score2.append(result21)
    precision_score2.append(result31)

    f1_score3.append(result12)
    recall_score3.append(result22)
    precision_score3.append(result32)

print("G1 F1 score - srednja vrijednost je: ", statistics.mean(f1_score1))
print("G1 F1 score - medijan je: ",statistics.median(f1_score1))
print("G1 F1 score - min je: ", min(f1_score1))
print("G1 F1 score - max je: ", max(f1_score1))
print("G1 F1 score - standardna devijacija je: ", statistics.stdev(f1_score1))
print(" ")
print("G1 Recall score - srednja vrijednost je: ", statistics.mean(recall_score1))
print("G1 Recall score - medijan je: ", statistics.median(recall_score1))
print("G1 Recall score - min je: ", min(recall_score1))
print("G1 Recall score - max je: ", max(recall_score1))
print("G1 Recall score - standardna devijacija je: ", statistics.stdev(f1_score1))
print(" ")
print("G1 Precision score - srednja vrijednost je: ", statistics.mean(precision_score1))
print("G1 Precision score - medijan je: ", statistics.median(precision_score1))
print("G1 Precision score - min je: ", min(precision_score1))
print("G1 Precision score - max je: ", max(precision_score1))
print("G1 Precision score - standardna devijacija je: ", statistics.stdev(precision_score1))
print("#################################################################")

print("G2 F1 score - srednja vrijednost je: ", statistics.mean(f1_score2))
print("G2 F1 score - medijan je: ",statistics.median(f1_score2))
print("G2 F1 score - min je: ", min(f1_score2))
print("G2 F1 score - max je: ", max(f1_score2))
print("G2 F1 score - standardna devijacija je: ", statistics.stdev(f1_score2))
print(" ")
print("G2 Recall score - srednja vrijednost je: ", statistics.mean(recall_score2))
print("G2 Recall score - medijan je: ", statistics.median(recall_score2))
print("G2 Recall score - min je: ", min(recall_score2))
print("G2 Recall score - max je: ", max(recall_score2))
print("G2 Recall score - standardna devijacija je: ", statistics.stdev(f1_score2))
print(" ")
print("G2 Precision score - srednja vrijednost je: ", statistics.mean(precision_score2))
print("G2 Precision score - medijan je: ", statistics.median(precision_score2))
print("G2 Precision score - min je: ", min(precision_score2))
print("G2 Precision score - max je: ", max(precision_score2))
print("G2 Precision score - standardna devijacija je: ", statistics.stdev(precision_score2))
print("#################################################################")

print("S1 F1 score - srednja vrijednost je: ", statistics.mean(f1_score3))
print("S1 F1 score - medijan je: ",statistics.median(f1_score3))
print("S1 F1 score - min je: ", min(f1_score3))
print("S1 F1 score - max je: ", max(f1_score3))
print("S1 F1 score - standardna devijacija je: ", statistics.stdev(f1_score3))
print(" ")
print("S1 Recall score - srednja vrijednost je: ", statistics.mean(recall_score3))
print("S1 Recall score - medijan je: ", statistics.median(recall_score3))
print("S1 Recall score - min je: ", min(recall_score3))
print("S1 Recall score - max je: ", max(recall_score3))
print("S1 Recall score - standardna devijacija je: ", statistics.stdev(f1_score3))
print(" ")
print("S1 Precision score - srednja vrijednost je: ", statistics.mean(precision_score3))
print("S1 Precision score - medijan je: ", statistics.median(precision_score3))
print("S1 Precision score - min je: ", min(precision_score3))
print("S1 Precision score - max je: ", max(precision_score3))
print("S1 Precision score - standardna devijacija je: ", statistics.stdev(precision_score3))
print("#################################################################")

row1 = 1
column1 = 3
row2 = 1
column2 = 3
row3 = 1
column3 = 3


for i in f1_score1:
    sheet1.write(row1,column1, i)
    row1 += 1

row1 += 1
sheet1.write(row1,column1, statistics.mean(f1_score1))
row1 += 1
sheet1.write(row1,column1, statistics.median(f1_score1))
row1 += 1
sheet1.write(row1,column1, min(f1_score1))
row1 += 1
sheet1.write(row1,column1, max(f1_score1))
row1 += 1
sheet1.write(row1,column1, statistics.stdev(f1_score1))
column1 += 1

for i in f1_score2:
    sheet2.write(row2,column2, i)
    row2 += 1

row2 += 1
sheet2.write(row2,column2, statistics.mean(f1_score2))
row2 += 1
sheet2.write(row2,column2, statistics.median(f1_score2))
row2 += 1
sheet2.write(row2,column2, min(f1_score2))
row2 += 1
sheet2.write(row2,column2, max(f1_score2))
row2 += 1
sheet2.write(row2,column2, statistics.stdev(f1_score2))
column2 += 1

for i in f1_score3:
    sheet3.write(row3,column3, i)
    row3 += 1

row3 += 1
sheet3.write(row3,column3, statistics.mean(f1_score3))
row3 += 1
sheet3.write(row3,column3, statistics.median(f1_score3))
row3 += 1
sheet3.write(row3,column3, min(f1_score3))
row3 += 1
sheet3.write(row3,column3, max(f1_score3))
row3 += 1
sheet3.write(row3,column3, statistics.stdev(f1_score3))
column3 += 1

print("DONE - KNN N=5!")


###########################################################################################################################################



f1_score1 = []
f1_score2 = []
f1_score3 = []
recall_score1 = []
recall_score2 = []
recall_score3 = []
precision_score1 = []
precision_score2 = []
precision_score3 = []

for i in range(30):
    X_train, X_test, y_train, y_test = train_test_split(X, y.values.ravel(), random_state=13+i, stratify=y)

    model = RandomForestClassifier()
    model.fit(X_train, y_train)
    
    y_pred = model.predict(X_test)
    y_pred1 = model.predict(X_G2)
    y_pred2 = model.predict(X_S1)
    
    result0 = model.score(X_test, y_test)
    result1 = f1_score(y_test, y_pred, average='macro', labels=np.unique(y_pred))
    result2 = recall_score(y_test, y_pred, average='macro', labels=np.unique(y_pred))
    result3 = precision_score(y_test, y_pred, average='macro', labels=np.unique(y_pred))
    print("RandomForest - Accuracy of iteration no.", i+1 ,": %.2f%%" % (result0*100.0))
    print("RandomForest - F1 Score of iteration no.", i+1 ,": %.2f%%" % (result1*100.0))
    print("RandomForest - Recall of iteration no.", i+1 ,": %.2f%%" % (result2*100.0))
    print("RandomForest - Precision of iteration no.", i+1 ,": %.2f%%" % (result3*100.0))
    print("##############################################################")
    result01 = model.score(X_G2, y_test_G2)
    result11 = f1_score(y_test_G2, y_pred1, average='macro', labels=np.unique(y_pred1))
    result21 = recall_score(y_test_G2, y_pred1, average='macro', labels=np.unique(y_pred1))
    result31 = precision_score(y_test_G2, y_pred1, average='macro', labels=np.unique(y_pred1))
    print("RandomForest - Accuracy of iteration no.", i+1 ,": %.2f%%" % (result01*100.0))
    print("RandomForest - F1 Score of iteration no.", i+1 ,": %.2f%%" % (result11*100.0))
    print("RandomForest - Recall of iteration no.", i+1 ,": %.2f%%" % (result21*100.0))
    print("RandomForest - Precision of iteration no.", i+1 ,": %.2f%%" % (result31*100.0))
    print("##############################################################")
    result02 = model.score(X_S1, y_test_S1)
    result12 = f1_score(y_test_S1, y_pred2, average='macro', labels=np.unique(y_pred2))
    result22 = recall_score(y_test_S1, y_pred2, average='macro', labels=np.unique(y_pred2))
    result32 = precision_score(y_test_S1, y_pred2, average='macro', labels=np.unique(y_pred2))
    print("RandomForest - Accuracy of iteration no.", i+1 ,": %.2f%%" % (result02*100.0))
    print("RandomForest - F1 Score of iteration no.", i+1 ,": %.2f%%" % (result12*100.0))
    print("RandomForest - Recall of iteration no.", i+1 ,": %.2f%%" % (result22*100.0))
    print("RandomForest - Precision of iteration no.", i+1 ,": %.2f%%" % (result32*100.0))
    print("##############################################################")
    f1_score1.append(result1)
    recall_score1.append(result2)
    precision_score1.append(result3)
    
    f1_score2.append(result11)
    recall_score2.append(result21)
    precision_score2.append(result31)

    f1_score3.append(result12)
    recall_score3.append(result22)
    precision_score3.append(result32)

print("G1 F1 score - srednja vrijednost je: ", statistics.mean(f1_score1))
print("G1 F1 score - medijan je: ",statistics.median(f1_score1))
print("G1 F1 score - min je: ", min(f1_score1))
print("G1 F1 score - max je: ", max(f1_score1))
print("G1 F1 score - standardna devijacija je: ", statistics.stdev(f1_score1))
print(" ")
print("G1 Recall score - srednja vrijednost je: ", statistics.mean(recall_score1))
print("G1 Recall score - medijan je: ", statistics.median(recall_score1))
print("G1 Recall score - min je: ", min(recall_score1))
print("G1 Recall score - max je: ", max(recall_score1))
print("G1 Recall score - standardna devijacija je: ", statistics.stdev(f1_score1))
print(" ")
print("G1 Precision score - srednja vrijednost je: ", statistics.mean(precision_score1))
print("G1 Precision score - medijan je: ", statistics.median(precision_score1))
print("G1 Precision score - min je: ", min(precision_score1))
print("G1 Precision score - max je: ", max(precision_score1))
print("G1 Precision score - standardna devijacija je: ", statistics.stdev(precision_score1))
print("#################################################################")

print("G2 F1 score - srednja vrijednost je: ", statistics.mean(f1_score2))
print("G2 F1 score - medijan je: ",statistics.median(f1_score2))
print("G2 F1 score - min je: ", min(f1_score2))
print("G2 F1 score - max je: ", max(f1_score2))
print("G2 F1 score - standardna devijacija je: ", statistics.stdev(f1_score2))
print(" ")
print("G2 Recall score - srednja vrijednost je: ", statistics.mean(recall_score2))
print("G2 Recall score - medijan je: ", statistics.median(recall_score2))
print("G2 Recall score - min je: ", min(recall_score2))
print("G2 Recall score - max je: ", max(recall_score2))
print("G2 Recall score - standardna devijacija je: ", statistics.stdev(f1_score2))
print(" ")
print("G2 Precision score - srednja vrijednost je: ", statistics.mean(precision_score2))
print("G2 Precision score - medijan je: ", statistics.median(precision_score2))
print("G2 Precision score - min je: ", min(precision_score2))
print("G2 Precision score - max je: ", max(precision_score2))
print("G2 Precision score - standardna devijacija je: ", statistics.stdev(precision_score2))
print("#################################################################")

print("S1 F1 score - srednja vrijednost je: ", statistics.mean(f1_score3))
print("S1 F1 score - medijan je: ",statistics.median(f1_score3))
print("S1 F1 score - min je: ", min(f1_score3))
print("S1 F1 score - max je: ", max(f1_score3))
print("S1 F1 score - standardna devijacija je: ", statistics.stdev(f1_score3))
print(" ")
print("S1 Recall score - srednja vrijednost je: ", statistics.mean(recall_score3))
print("S1 Recall score - medijan je: ", statistics.median(recall_score3))
print("S1 Recall score - min je: ", min(recall_score3))
print("S1 Recall score - max je: ", max(recall_score3))
print("S1 Recall score - standardna devijacija je: ", statistics.stdev(f1_score3))
print(" ")
print("S1 Precision score - srednja vrijednost je: ", statistics.mean(precision_score3))
print("S1 Precision score - medijan je: ", statistics.median(precision_score3))
print("S1 Precision score - min je: ", min(precision_score3))
print("S1 Precision score - max je: ", max(precision_score3))
print("S1 Precision score - standardna devijacija je: ", statistics.stdev(precision_score3))
print("#################################################################")


row1 = 1
column1 = 4
row2 = 1
column2 = 4
row3 = 1
column3 = 4


for i in f1_score1:
    sheet1.write(row1,column1, i)
    row1 += 1

row1 += 1
sheet1.write(row1,column1, statistics.mean(f1_score1))
row1 += 1
sheet1.write(row1,column1, statistics.median(f1_score1))
row1 += 1
sheet1.write(row1,column1, min(f1_score1))
row1 += 1
sheet1.write(row1,column1, max(f1_score1))
row1 += 1
sheet1.write(row1,column1, statistics.stdev(f1_score1))
column1 += 1

for i in f1_score2:
    sheet2.write(row2,column2, i)
    row2 += 1

row2 += 1
sheet2.write(row2,column2, statistics.mean(f1_score2))
row2 += 1
sheet2.write(row2,column2, statistics.median(f1_score2))
row2 += 1
sheet2.write(row2,column2, min(f1_score2))
row2 += 1
sheet2.write(row2,column2, max(f1_score2))
row2 += 1
sheet2.write(row2,column2, statistics.stdev(f1_score2))
column2 += 1

for i in f1_score3:
    sheet3.write(row3,column3, i)
    row3 += 1

row3 += 1
sheet3.write(row3,column3, statistics.mean(f1_score3))
row3 += 1
sheet3.write(row3,column3, statistics.median(f1_score3))
row3 += 1
sheet3.write(row3,column3, min(f1_score3))
row3 += 1
sheet3.write(row3,column3, max(f1_score3))
row3 += 1
sheet3.write(row3,column3, statistics.stdev(f1_score3))
column3 += 1

print("DONE - Random Forest!")



###########################################################################################################################################


f1_score1 = []
f1_score2 = []
f1_score3 = []
recall_score1 = []
recall_score2 = []
recall_score3 = []
precision_score1 = []
precision_score2 = []
precision_score3 = []

for i in range(30):
    X_train, X_test, y_train, y_test = train_test_split(X, y.values.ravel(), random_state=13+i, stratify=y)

    model = DecisionTreeClassifier()
    model.fit(X_train, y_train)
    
    y_pred = model.predict(X_test)
    y_pred1 = model.predict(X_G2)
    y_pred2 = model.predict(X_S1)
    
    result0 = model.score(X_test, y_test)
    result1 = f1_score(y_test, y_pred, average='macro', labels=np.unique(y_pred))
    result2 = recall_score(y_test, y_pred, average='macro', labels=np.unique(y_pred))
    result3 = precision_score(y_test, y_pred, average='macro', labels=np.unique(y_pred))
    print("DecisionTree - Accuracy of iteration no.", i+1 ,": %.2f%%" % (result0*100.0))
    print("DecisionTree - F1 Score of iteration no.", i+1 ,": %.2f%%" % (result1*100.0))
    print("DecisionTree - Recall of iteration no.", i+1 ,": %.2f%%" % (result2*100.0))
    print("DecisionTree - Precision of iteration no.", i+1 ,": %.2f%%" % (result3*100.0))
    print("##############################################################")
    result01 = model.score(X_G2, y_test_G2)
    result11 = f1_score(y_test_G2, y_pred1, average='macro', labels=np.unique(y_pred1))
    result21 = recall_score(y_test_G2, y_pred1, average='macro', labels=np.unique(y_pred1))
    result31 = precision_score(y_test_G2, y_pred1, average='macro', labels=np.unique(y_pred1))
    print("DecisionTree - Accuracy of iteration no.", i+1 ,": %.2f%%" % (result01*100.0))
    print("DecisionTree - F1 Score of iteration no.", i+1 ,": %.2f%%" % (result11*100.0))
    print("DecisionTree - Recall of iteration no.", i+1 ,": %.2f%%" % (result21*100.0))
    print("DecisionTree - Precision of iteration no.", i+1 ,": %.2f%%" % (result31*100.0))
    print("##############################################################")
    result02 = model.score(X_S1, y_test_S1)
    result12 = f1_score(y_test_S1, y_pred2, average='macro', labels=np.unique(y_pred2))
    result22 = recall_score(y_test_S1, y_pred2, average='macro', labels=np.unique(y_pred2))
    result32 = precision_score(y_test_S1, y_pred2, average='macro', labels=np.unique(y_pred2))
    print("DecisionTree - Accuracy of iteration no.", i+1 ,": %.2f%%" % (result02*100.0))
    print("DecisionTree - F1 Score of iteration no.", i+1 ,": %.2f%%" % (result12*100.0))
    print("DecisionTree - Recall of iteration no.", i+1 ,": %.2f%%" % (result22*100.0))
    print("DecisionTree - Precision of iteration no.", i+1 ,": %.2f%%" % (result32*100.0))
    print("##############################################################")
    f1_score1.append(result1)
    recall_score1.append(result2)
    precision_score1.append(result3)
    
    f1_score2.append(result11)
    recall_score2.append(result21)
    precision_score2.append(result31)

    f1_score3.append(result12)
    recall_score3.append(result22)
    precision_score3.append(result32)

print("G1 F1 score - srednja vrijednost je: ", statistics.mean(f1_score1))
print("G1 F1 score - medijan je: ",statistics.median(f1_score1))
print("G1 F1 score - min je: ", min(f1_score1))
print("G1 F1 score - max je: ", max(f1_score1))
print("G1 F1 score - standardna devijacija je: ", statistics.stdev(f1_score1))
print(" ")
print("G1 Recall score - srednja vrijednost je: ", statistics.mean(recall_score1))
print("G1 Recall score - medijan je: ", statistics.median(recall_score1))
print("G1 Recall score - min je: ", min(recall_score1))
print("G1 Recall score - max je: ", max(recall_score1))
print("G1 Recall score - standardna devijacija je: ", statistics.stdev(f1_score1))
print(" ")
print("G1 Precision score - srednja vrijednost je: ", statistics.mean(precision_score1))
print("G1 Precision score - medijan je: ", statistics.median(precision_score1))
print("G1 Precision score - min je: ", min(precision_score1))
print("G1 Precision score - max je: ", max(precision_score1))
print("G1 Precision score - standardna devijacija je: ", statistics.stdev(precision_score1))
print("#################################################################")

print("G2 F1 score - srednja vrijednost je: ", statistics.mean(f1_score2))
print("G2 F1 score - medijan je: ",statistics.median(f1_score2))
print("G2 F1 score - min je: ", min(f1_score2))
print("G2 F1 score - max je: ", max(f1_score2))
print("G2 F1 score - standardna devijacija je: ", statistics.stdev(f1_score2))
print(" ")
print("G2 Recall score - srednja vrijednost je: ", statistics.mean(recall_score2))
print("G2 Recall score - medijan je: ", statistics.median(recall_score2))
print("G2 Recall score - min je: ", min(recall_score2))
print("G2 Recall score - max je: ", max(recall_score2))
print("G2 Recall score - standardna devijacija je: ", statistics.stdev(f1_score2))
print(" ")
print("G2 Precision score - srednja vrijednost je: ", statistics.mean(precision_score2))
print("G2 Precision score - medijan je: ", statistics.median(precision_score2))
print("G2 Precision score - min je: ", min(precision_score2))
print("G2 Precision score - max je: ", max(precision_score2))
print("G2 Precision score - standardna devijacija je: ", statistics.stdev(precision_score2))
print("#################################################################")

print("S1 F1 score - srednja vrijednost je: ", statistics.mean(f1_score3))
print("S1 F1 score - medijan je: ",statistics.median(f1_score3))
print("S1 F1 score - min je: ", min(f1_score3))
print("S1 F1 score - max je: ", max(f1_score3))
print("S1 F1 score - standardna devijacija je: ", statistics.stdev(f1_score3))
print(" ")
print("S1 Recall score - srednja vrijednost je: ", statistics.mean(recall_score3))
print("S1 Recall score - medijan je: ", statistics.median(recall_score3))
print("S1 Recall score - min je: ", min(recall_score3))
print("S1 Recall score - max je: ", max(recall_score3))
print("S1 Recall score - standardna devijacija je: ", statistics.stdev(f1_score3))
print(" ")
print("S1 Precision score - srednja vrijednost je: ", statistics.mean(precision_score3))
print("S1 Precision score - medijan je: ", statistics.median(precision_score3))
print("S1 Precision score - min je: ", min(precision_score3))
print("S1 Precision score - max je: ", max(precision_score3))
print("S1 Precision score - standardna devijacija je: ", statistics.stdev(precision_score3))
print("#################################################################")

row1 = 1
column1 = 5
row2 = 1
column2 = 5
row3 = 1
column3 = 5


for i in f1_score1:
    sheet1.write(row1,column1, i)
    row1 += 1

row1 += 1
sheet1.write(row1,column1, statistics.mean(f1_score1))
row1 += 1
sheet1.write(row1,column1, statistics.median(f1_score1))
row1 += 1
sheet1.write(row1,column1, min(f1_score1))
row1 += 1
sheet1.write(row1,column1, max(f1_score1))
row1 += 1
sheet1.write(row1,column1, statistics.stdev(f1_score1))
column1 += 1

for i in f1_score2:
    sheet2.write(row2,column2, i)
    row2 += 1

row2 += 1
sheet2.write(row2,column2, statistics.mean(f1_score2))
row2 += 1
sheet2.write(row2,column2, statistics.median(f1_score2))
row2 += 1
sheet2.write(row2,column2, min(f1_score2))
row2 += 1
sheet2.write(row2,column2, max(f1_score2))
row2 += 1
sheet2.write(row2,column2, statistics.stdev(f1_score2))
column2 += 1

for i in f1_score3:
    sheet3.write(row3,column3, i)
    row3 += 1

row3 += 1
sheet3.write(row3,column3, statistics.mean(f1_score3))
row3 += 1
sheet3.write(row3,column3, statistics.median(f1_score3))
row3 += 1
sheet3.write(row3,column3, min(f1_score3))
row3 += 1
sheet3.write(row3,column3, max(f1_score3))
row3 += 1
sheet3.write(row3,column3, statistics.stdev(f1_score3))
column3 += 1

print("DONE - Decision Tree!")

book1.close()
book2.close()
book3.close()


loc = ("Rezultati1.xlsx")
wb = xlrd.open_workbook(loc)
sheet = wb.sheet_by_index(0)
box1 = []
for i in range(1, 30):
    box1.append(sheet.cell_value(i, 1))

box2 = []
for i in range(1, 30):
    box2.append(sheet.cell_value(i, 2))

box3 = []
for i in range(1, 30):
    box3.append(sheet.cell_value(i, 3))

box4 = []
for i in range(1, 30):
    box4.append(sheet.cell_value(i, 4))

box5 = []
for i in range(1, 30):
    box5.append(sheet.cell_value(i, 5))

print(box1)
print(box2)
print(box3)
print(box4)
print(box5)

data=[box1, box2, box3, box4, box5]
plt.boxplot(data)
plt.show()

############################################################


loc = ("Rezultati2.xlsx")
wb = xlrd.open_workbook(loc)
sheet = wb.sheet_by_index(0)
box1 = []
for i in range(1, 30):
    box1.append(sheet.cell_value(i, 1))

box2 = []
for i in range(1, 30):
    box2.append(sheet.cell_value(i, 2))

box3 = []
for i in range(1, 30):
    box3.append(sheet.cell_value(i, 3))

box4 = []
for i in range(1, 30):
    box4.append(sheet.cell_value(i, 4))

box5 = []
for i in range(1, 30):
    box5.append(sheet.cell_value(i, 5))

data=[box1, box2, box3, box4, box5]
plt.boxplot(data)
plt.show()

############################################################

loc = ("Rezultati3.xlsx")
wb = xlrd.open_workbook(loc)
sheet = wb.sheet_by_index(0)
box1 = []
for i in range(1, 30):
    box1.append(sheet.cell_value(i, 1))

box2 = []
for i in range(1, 30):
    box2.append(sheet.cell_value(i, 2))

box3 = []
for i in range(1, 30):
    box3.append(sheet.cell_value(i, 3))

box4 = []
for i in range(1, 30):
    box4.append(sheet.cell_value(i, 4))

box5 = []
for i in range(1, 30):
    box5.append(sheet.cell_value(i, 5))


data=[box1, box2, box3, box4, box5]
plt.boxplot(data)
plt.show()

############################################################


